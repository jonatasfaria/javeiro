package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JaveiroWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(JaveiroWebApplication.class, args);
	}
}
