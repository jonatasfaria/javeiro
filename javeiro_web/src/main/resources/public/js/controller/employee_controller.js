'use strict';

angular.module('myApp').controller('EmployeeController', ['$scope', 'EmployeeService','$q' , function($scope, EmployeeService, $q) {
    var self = this;
    self.employee={
    		empNo:null,
    		firstName:'',
    		lastName:'',
    		birthDate: new Date(),
    		hireDate: new Date(),
    		gender:'M'};
    
    self.employees=[];
    self.filter = '';
    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.search = search;
    self.onPaginate = onPaginate;
    
    self.limitOptions = [ 5, 10, 15, 20, 25, 50, 100 ];
    self.pageable = {
			size : 5,
			page : 1
		};
    
    fetchAllEmployees();
    
    function fetchAllEmployees(filter){
    	var pageable = angular.copy(self.pageable);
    	
    	pageable.page = pageable.page - 1;
    	
    	$scope.promise = EmployeeService.fetchAllEmployees(pageable, filter)
            .then(
            function(d) {
                self.employees = d.content;
                self.totalElements = d.totalElements;
            },
            function(errResponse){
                console.error('Error while fetching Employees');
            }
        );
    }
    
    function onPaginate(page, limit) {
    	fetchAllEmployees();
	}

    function createEmployee(employee){
        EmployeeService.createEmployee(employee)
            .then(
            fetchAllEmployees(null),
            function(errResponse){
                console.error('Error while creating Employee');
            }
        );
    }

    function updateEmployee(employee, id){
        EmployeeService.updateEmployee(employee, id)
            .then(
            fetchAllEmployees(null),
            function(errResponse){
                console.error('Error while updating Employee');
            }
        );
    }

    function deleteEmployee(id){
        EmployeeService.deleteEmployee(id)
            .then(
            fetchAllEmployees(null),
            function(errResponse){
                console.error('Error while deleting Employee');
            }
        );
    }

    function submit() {
        if(self.employee.empNo===null){
            console.log('Saving New Employee', self.employee);
            createEmployee(self.employee);
        }else{
            updateEmployee(self.employee, self.employee.empNo);
            console.log('Employee updated with id ', self.employee.empNo);
        }
        reset();
    }

    function edit(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.employees.length; i++){
            if(self.employees[i].empNo === id) {
                self.employee = angular.copy(self.employees[i]);
                self.employee.birthDate = moment(self.employees[i].birthDate)._d;
                self.employee.hireDate = moment(self.employees[i].hireDate)._d;
                break;
            }
        }
    }

    function remove(id){
        console.log('id to be deleted', id);
        if(self.employee.empNo === id) {//clean form if the employee to be deleted is shown there.
            reset();
        }
        deleteEmployee(id);
    }


    function reset(){
        self.employee={
        		empNo:null,
        		firstName:'',
        		lastName:'',
        		birthDate: null,
        		hireDate: null,
        		gender:'M'
        		};
        
        $scope.myForm.$setPristine(); //reset Form
    }
    
    function search(){
    	fetchAllEmployees(self.filter);
    } 

}]);
