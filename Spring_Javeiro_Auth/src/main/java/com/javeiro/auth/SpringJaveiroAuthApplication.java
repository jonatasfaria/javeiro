package com.javeiro.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class SpringJaveiroAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJaveiroAuthApplication.class, args);
	}
}
