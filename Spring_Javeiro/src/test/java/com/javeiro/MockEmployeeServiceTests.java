package com.javeiro;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.javeiro.model.Employees;
import com.javeiro.repository.EmployeeRepository;
import com.javeiro.service.EmployeeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MockEmployeeServiceTests {
	
	@Autowired
	@InjectMocks
	private EmployeeService mockEmployeeService;
	
	@Mock
    private EmployeeRepository mockEmployeeRepository;

	@Mock
	private Employees mockEmployee;

	@Before
    public void setup() {
		MockitoAnnotations.initMocks(this);
    }
	
	@Test
	public void findById() {
		when(mockEmployeeRepository.findOne(anyInt())).thenReturn(mockEmployee);

		int empNo = 1;

		Employees employeeFind = mockEmployeeService.findById(empNo);
		assertTrue(mockEmployee == employeeFind);
	}

	@Test
	public void created() {
		when(mockEmployeeRepository.saveAndFlush(any(Employees.class))).thenReturn(mockEmployee);

		Employees employeeCreated = mockEmployeeService.create(mockEmployee);

		assertTrue(mockEmployee == employeeCreated);
	}
		
	@Test(expected = IllegalArgumentException.class)
	public void validateEmployee() throws Exception {
		int empNo = 1;
		
		when(mockEmployeeRepository.exists(anyInt())).thenReturn(false);
		
		mockEmployeeService.validateEmployee(empNo);
	}
}