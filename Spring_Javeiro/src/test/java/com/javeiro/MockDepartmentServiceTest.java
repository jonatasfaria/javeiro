package com.javeiro;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.javeiro.model.Departments;
import com.javeiro.repository.DepartmentsRepository;
import com.javeiro.service.DepartmentsService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MockDepartmentServiceTest {

	@Autowired
	@InjectMocks
	private DepartmentsService departmentService;

	@Autowired
	private DepartmentsRepository departmentRepository;

	@Mock
	private Departments department;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void findById() {
		when(departmentRepository.findOne(anyInt())).thenReturn(department);

		int id = 1;

		Departments departmentFind = departmentService.findById(id);
		assertTrue(department == departmentFind);
	}

	@Test
	public void created() {
		when(departmentRepository.saveAndFlush(any(Departments.class))).thenReturn(department);

		Departments departmentCreated = departmentService.create(department);

		assertTrue(department == departmentCreated);
	}

}