package com.javeiro.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "titles")
public class Titles implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="emp_no", referencedColumnName="emp_no", insertable=false, updatable=false)
	@JsonIgnore
	private Employees employee;
	
	@Column(name = "from_date")
	private Date fromDate;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "title")
	private String title;

	@Column(name = "to_date")
	private Date toDate;
	
	public int getId() {
		return id;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public String getTitle() {
		return title;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setEmployee(Employees employee) {
		this.employee = employee;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
}
