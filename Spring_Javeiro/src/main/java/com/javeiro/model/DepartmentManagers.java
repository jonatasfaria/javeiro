package com.javeiro.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "dept_man")
public class DepartmentManagers implements Serializable {

		private static final long serialVersionUID = 1L;
		@ManyToOne
		@JoinColumn(name = "dept_no", referencedColumnName = "dept_id", insertable = false, updatable = false)
		@JsonIgnore
		private Departments department;
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		@Column(name = "dept_id_man", unique = true, nullable = false)
		private int dept_id_man;
		@ManyToOne
		@JoinColumn(name = "emp_no", referencedColumnName = "emp_no", insertable = false, updatable = false)
		@JsonIgnore
		private Employees employee;
		@Column(name = "from_date", nullable = false)
		private Date fromDate;
		@Column(name = "to_date")
		private Date toDate;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

		public Departments getDepartment() {
			return department;
		}

		public void setDepartment(Departments department) {
			this.department = department;
		}

	public int getDept_id_man() {
		return dept_id_man;
	}

		public void setDept_id_man(int dept_id_man) {
			this.dept_id_man = dept_id_man;
		}

	public Employees getEmployee() {
		return employee;
	}

		public void setEmployee(Employees employee) {
			this.employee = employee;
		}

	public Date getFromDate() {
		return fromDate;
	}

		public void setFromDate(Date fromDate) {
			this.fromDate = fromDate;
		}

	public Date getToDate() {
		return toDate;
		}

		public void setToDate(Date toDate) {
			this.toDate = toDate;
		}
	}
