package com.javeiro.model;

public class Report {

	private String gender;

	private double average;

	public Report() {
		// TODO Auto-generated constructor stub
	}
	
	public Report(String gender, double average) {
		super();
		this.gender = gender;
		this.average = average;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public double getAverage() {
		return average;
	}

	public void setAverage(double average) {
		this.average = average;
	}
}
