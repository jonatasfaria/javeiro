package com.javeiro.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "employees")
public class Employees {

	@Column(name = "birth_date")
	private Date birthDate;

	@OneToMany(orphanRemoval = false, cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "emp_id", updatable = false, referencedColumnName="emp_no", nullable = false)
	private List<DepartEmployees> departEmployees = new ArrayList<DepartEmployees>();

	@OneToMany(orphanRemoval = false, cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "emp_no", nullable = false)
	private List<DepartmentManagers> departmentManager = new ArrayList<DepartmentManagers>();

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "emp_no", unique = true, nullable = false)
	private Integer empNo;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "gender")
	private String gender;

	@Column(name = "hire_date")
	private Date hireDate;

	@Column(name = "last_name")
	private String lastName;

	@OneToMany(orphanRemoval = false, cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "emp_no", nullable = false)
	private List<Salaries> salaries = new ArrayList<Salaries>();

	@OneToMany(orphanRemoval = false, cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "emp_no", nullable = false)
	private List<Titles> titles;

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public List<DepartEmployees> getDepartEmployees() {
		return departEmployees;
	}

	public void setDepartEmployees(List<DepartEmployees> departEmployees) {
		this.departEmployees = departEmployees;
	}

	public List<DepartmentManagers> getDepartmentManager() {
		return departmentManager;
	}

	public void setDepartmentManager(List<DepartmentManagers> departmentManager) {
		this.departmentManager = departmentManager;
	}

	public Integer getEmpNo() {
		return empNo;
	}

	public void setEmpNo(Integer empNo) {
		this.empNo = empNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Salaries> getSalaries() {
		return salaries;
	}

	public void setSalaries(List<Salaries> salaries) {
		this.salaries = salaries;
	}

	public List<Titles> getTitles() {
		return titles;
	}

	public void setTitles(List<Titles> titles) {
		this.titles = titles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((empNo == null) ? 0 : empNo.hashCode());
		return result;
	}
}
