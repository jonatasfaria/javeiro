package com.javeiro.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "departments")
@JsonIgnoreProperties({ "departEmployees", "departmentManager" })
public class Departments {

	@OneToMany(orphanRemoval = false, cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "dept_no", updatable = false, nullable = false)
	private List<DepartEmployees> departEmployees;

	@OneToMany(orphanRemoval = false, cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "dept_no", nullable = false)
	private List<DepartmentManagers> departmentManager;

	@Column(name = "dept_name")
	private String deptName;
	@Column(name = "dept_no", unique = true, nullable = false)
	private String deptNo;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "dept_id", unique = true, nullable = false)
	private int id;

	public List<DepartmentManagers> getDepartmentManager() {
		return departmentManager;
	}

	public void setDepartmentManager(List<DepartmentManagers> departmentManager) {
		this.departmentManager = departmentManager;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<DepartEmployees> getDepartEmployees() {
		return departEmployees;
	}

	public void setDepartEmployees(List<DepartEmployees> departEmployees) {
		this.departEmployees = departEmployees;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}
}