package com.javeiro.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "dept_emp")
public class DepartEmployees implements Serializable {

	private static final long serialVersionUID = 1L;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@ManyToOne
	@JoinColumn(name = "dept_no", referencedColumnName = "dept_id", insertable = false, updatable = false)
	@JsonIgnore
	private Departments department;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "dept_id_emp", unique = true, nullable = false)
	private int dept_id_emp;

	@ManyToOne
	@JoinColumn(name = "emp_id", referencedColumnName = "emp_no", insertable = false, updatable = false)
	@JsonIgnore
	private Employees employee;

	@Column(name = "from_date", nullable = false)
	private Date fromDate;

	@Column(name = "to_date")
	private Date toDate;

	public Departments getDepartment() {
		return department;
	}

	public int getDept_id_emp() {
		return dept_id_emp;
	}

	public Employees getEmployee() {
		return employee;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setDepartment(Departments department) {
		this.department = department;
	}

	public void setDept_id_emp(int dept_id_emp) {
		this.dept_id_emp = dept_id_emp;
	}

	public void setEmployee(Employees employee) {
		this.employee = employee;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
}
