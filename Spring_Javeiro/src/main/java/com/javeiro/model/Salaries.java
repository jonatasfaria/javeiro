package com.javeiro.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "salaries")
public class Salaries implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="emp_no", referencedColumnName="emp_no", insertable=false, updatable=false)
	@JsonIgnore
	private Employees employee;

	@Column(name = "from_date", nullable = false)
	private Date fromDate;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private int id;

	@Column(name = "salary")
	private Integer salary;

	@Column(name = "to_date")
	private Date toDate;

	public Date getFromDate() {
		return fromDate;
	}

	public int getId() {
		return id;
	}

	public Integer getSalary() {
		return salary;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setEmployee(Employees employee) {
		this.employee = employee;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
}
