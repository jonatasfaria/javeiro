package com.javeiro.config;

import com.javeiro.DTO.Department;
import com.javeiro.DTO.Employee;
import com.javeiro.model.DepartEmployees;
import com.javeiro.model.DepartmentManagers;
import com.javeiro.model.Departments;
import com.javeiro.model.Employees;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;

import java.util.ArrayList;
import java.util.List;

public class Map {

	private static ModelMapper modelMapper;

	private static Converter<Departments, Department> getDepartmentMap() {
		return new Converter<Departments, Department>() {
			public Department convert(MappingContext<Departments, Department> context) {

				Department departmentDTO = context.getDestination();
				Departments department = context.getSource();

				List<Employees> employees = new ArrayList<Employees>();

				for (DepartEmployees departEmployee : department.getDepartEmployees()) {
					employees.add(departEmployee.getEmployee());
				}

				departmentDTO.setEmployees(employees);

				List<Employees> managers = new ArrayList<Employees>();

				for (DepartmentManagers departManager : department.getDepartmentManager()) {
					managers.add(departManager.getEmployee());
				}

				departmentDTO.setManagers(managers);

				return departmentDTO;
			}
		};
	}

	private static Converter<Employees, Employee> getEmployeeMap() {
		return new Converter<Employees, Employee>() {
			public Employee convert(MappingContext<Employees, Employee> context) {

				Employees employee = context.getSource();
				Employee employeeDTO = context.getDestination();

				List<Departments> departmentsEmployees = new ArrayList<Departments>();

				for (DepartEmployees departEmployee : employee.getDepartEmployees()) {
					departmentsEmployees.add(departEmployee.getDepartment());
				}

				employeeDTO.setDepartmentsEmployee(departmentsEmployees);

				List<Departments> departManagers = new ArrayList<Departments>();

				for (DepartmentManagers departManager : employee.getDepartmentManager()) {
					departManagers.add(departManager.getDepartment());
				}

				employeeDTO.setDepartmentsManager(departManagers);

				return employeeDTO;
			}
		};
	}

	public static ModelMapper getModelMapper() {
		return modelMapper;
	}

	public static void setModelMapper(ModelMapper modelMapper) {
		Map.modelMapper = modelMapper;
	}

	public static void init() {
		Map.modelMapper = new ModelMapper();

		Map.modelMapper.createTypeMap(Departments.class, Department.class).setPostConverter(getDepartmentMap());
		Map.modelMapper.createTypeMap(Employees.class, Employee.class).setPostConverter(getEmployeeMap());
	}
}
