package com.javeiro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.javeiro.config.Map;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
//@EnableResourceServer
public class SpringJaveiroApplication {

	public static void main(String[] args) {
		Map.init();
		SpringApplication.run(SpringJaveiroApplication.class, args);
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(
				RequestHandlerSelectors.basePackage("com.javeiro.controller"))
				.paths(PathSelectors.any()).build();
	}
	
	@Bean
	   public WebMvcConfigurer corsConfigurer() {
	       return new WebMvcConfigurerAdapter() {
	           @Override
	           public void addCorsMappings(CorsRegistry registry) {
	        	   registry.addMapping("/v1/employees/").allowedOrigins("*");
	        	   registry.addMapping("/api/v1/employees/").allowedOrigins("*");
	           }
	       };
	   }
}
