package com.javeiro.service;

import java.util.List;

import com.javeiro.model.Report;

public interface ReportService {

	List<Report> getAverageSalaryByGender();
}
