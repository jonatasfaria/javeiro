package com.javeiro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javeiro.model.Titles;
import com.javeiro.repository.TitleRepository;

@Service
public class TitleServiceImpl implements TitleService {

	@Autowired
	private TitleRepository titleRepository;

	@Autowired
	private EmployeeService employeeService;

	@Override
	public void delete(int id) {
		this.titleRepository.delete(id);
	}

	@Override
	public Titles get(int empId, int id) {
		List<Titles> titles = this.employeeService.findById(empId).getTitles();
		return titles.stream().filter(q -> q.getId() == id).findFirst().orElse(null);
	}

	@Override
	public Titles update(Titles title) {
		return this.titleRepository.saveAndFlush(title);
	}

	@Override
	public List<Titles> getTitleByEmployee(int empId) {
		return this.employeeService.findById(empId).getTitles();
	}
}
