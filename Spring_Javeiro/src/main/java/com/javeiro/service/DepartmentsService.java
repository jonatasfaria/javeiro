package com.javeiro.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.javeiro.model.DepartEmployees;
import com.javeiro.model.Departments;
import com.javeiro.model.Employees;

public interface DepartmentsService {

	Departments create(Departments department);

	void delete(int id);

	Boolean exists(int id);

	Page<Departments> findAll(Pageable pageable);

	Departments findById(int id);

	Employees getEmployee(int deptId, int empId);

	List<Employees> getEmployees(int deptId);
	
	List<Employees> getManagers(int deptId);

	Departments update(Departments department);

	void validate(int deptId) throws Exception;
}
