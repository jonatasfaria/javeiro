package com.javeiro.service;

import com.javeiro.model.DepartmentManagers;
import com.javeiro.model.Employees;

import java.util.List;

/**
 * Created by erics on 27/09/2016.
 */
public interface DepartmentManagerService {
    DepartmentManagers create(int deptId, DepartmentManagers departmentManager);

    void deleteByDepartment(int deptId);

    void deleteByEmployeeAndDepartment(int empId, int deptId);

    Employees findByEmployeeAndDepartment(int empId, int deptId);

    List<Employees> findByDepartment(int deptId);

    List<DepartmentManagers> update(int deptId, DepartmentManagers departmentManager);

    DepartmentManagers update(int deptId, int empId, DepartmentManagers departmentManager);
}
