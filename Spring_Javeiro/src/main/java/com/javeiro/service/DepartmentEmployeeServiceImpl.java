package com.javeiro.service;

import com.javeiro.model.DepartEmployees;
import com.javeiro.model.Departments;
import com.javeiro.model.Employees;
import com.javeiro.repository.DepartmentEmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentEmployeeServiceImpl implements DepartmentEmployeeService {

	@Autowired
	private DepartmentEmployeeRepository departmentEmployeeRepository;

	@Autowired
	private DepartmentsService departmentsService;

	@Autowired
	private EmployeeService employeeService;

	@Override
	public DepartEmployees create(int deptId, DepartEmployees departEmp) {
		Departments depart = this.departmentsService.findById(deptId);
		Employees emp = this.employeeService.findById(departEmp.getEmployee().getEmpNo());

		departEmp.setDepartment(depart);
		departEmp.setEmployee(emp);

		List<DepartEmployees> deptEmps = depart.getDepartEmployees();
		deptEmps.add(departEmp);
//		depart.setDepartEmployees(deptEmps);

		List<DepartEmployees> empDepts = emp.getDepartEmployees();
		empDepts.add(departEmp);
//		emp.setDepartEmployees(empDepts);

		DepartEmployees deptEmpResult = this.departmentEmployeeRepository.saveAndFlush(departEmp);
//		this.employeeService.update(emp);
//		Departments update = this.departmentsService.update(depart);
		return deptEmpResult;
	}

	@Override
	public void deleteByDepartment(int deptId) {
		Departments department = new Departments();
		department.setId(deptId);

		this.departmentEmployeeRepository.deleteByDepartment(department);
	}

	@Override
	public void deleteByEmployeeAndDepartment(int empId, int deptId) {
		Employees emp = new Employees();
		emp.setEmpNo(empId);

		Departments department = new Departments();
		department.setId(deptId);

		this.departmentEmployeeRepository.deleteByEmployeeAndDepartment(emp, department);
	}

	@Override
	public Employees findByEmployeeAndDepartment(int empId, int deptId) {
		Employees emp = new Employees();
		emp.setEmpNo(empId);

		Departments dept = new Departments();
		dept.setId(deptId);

		List<DepartEmployees> deptsEmp = this.departmentEmployeeRepository.findByEmployeeAndDepartment(emp, dept);
		return deptsEmp.stream().findFirst().get().getEmployee();
	}

	@Override
	public List<Employees> findByDepartment(int deptId) {
		Departments dept = new Departments();
		dept.setId(deptId);

		List<DepartEmployees> deptsEmp = this.departmentEmployeeRepository.findByDepartment(dept);
		return deptsEmp.stream().map(q -> q.getEmployee()).collect(Collectors.toList());
	}

	@Override
	public List<DepartEmployees> update(int deptId, DepartEmployees departEmp) {

		List<DepartEmployees> updatedDepartmentEmployees = new ArrayList<DepartEmployees>();

		Departments depart = this.departmentsService.findById(deptId);

		for (DepartEmployees departEmployees : this.departmentEmployeeRepository.findByDepartment(depart)) {

			departEmployees.setFromDate(departEmp.getFromDate());
			departEmployees.setToDate(departEmp.getToDate());

			Employees emp = this.employeeService.findById(departEmployees.getEmployee().getEmpNo());

			departEmployees.setDepartment(depart);
			departEmployees.setEmployee(emp);

			List<DepartEmployees> deptEmps = depart.getDepartEmployees();
			deptEmps.add(departEmployees);
			depart.setDepartEmployees(deptEmps);

			List<DepartEmployees> empDepts = emp.getDepartEmployees();
			empDepts.add(departEmployees);
			emp.setDepartEmployees(empDepts);

			updatedDepartmentEmployees.add(this.departmentEmployeeRepository.saveAndFlush(departEmployees));
		}

		return updatedDepartmentEmployees;
	}

	@Override
	public DepartEmployees update(int deptId, int empId, DepartEmployees departEmp) {

		Departments depart = this.departmentsService.findById(deptId);
		Employees emp = this.employeeService.findById(departEmp.getEmployee().getEmpNo());

		DepartEmployees departEmployees = this.departmentEmployeeRepository.findByEmployeeAndDepartment(emp, depart)
				.stream().findFirst().orElse(null);

		departEmployees.setFromDate(departEmp.getFromDate());
		departEmployees.setToDate(departEmp.getToDate());

		departEmployees.setDepartment(depart);
		departEmployees.setEmployee(emp);

		List<DepartEmployees> deptEmps = depart.getDepartEmployees();
		deptEmps.add(departEmployees);
		depart.setDepartEmployees(deptEmps);

		List<DepartEmployees> empDepts = emp.getDepartEmployees();
		empDepts.add(departEmployees);
		emp.setDepartEmployees(empDepts);

		return this.departmentEmployeeRepository.saveAndFlush(departEmployees);
	}
}
