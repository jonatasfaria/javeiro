package com.javeiro.service;

import java.util.List;

import com.javeiro.model.Titles;

public interface TitleService {
	void delete(int id);

	Titles get(int empId, int id);

	Titles update(Titles title);

	List<Titles> getTitleByEmployee(int empId);
}
