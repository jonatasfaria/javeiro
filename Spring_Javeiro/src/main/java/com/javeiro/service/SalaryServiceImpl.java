package com.javeiro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javeiro.model.Salaries;
import com.javeiro.repository.SalaryRepository;

@Service
public class SalaryServiceImpl implements SalaryService {

	@Autowired
	private SalaryRepository salaryRepository;

	@Autowired
	private EmployeeService employeeService;

	@Override
	public void delete(int id) {
		this.salaryRepository.delete(id);
	}

	public Salaries get(int empId, int id) {
		List<Salaries> salaries = this.employeeService.findById(empId).getSalaries();
		return salaries.stream().filter(q -> q.getId() == id).findFirst().orElse(null);
	}

	public Salaries get(int id) {
		return this.salaryRepository.findOne(id);
	}

	@Override
	public List<Salaries> getSalaryByEmployee(int empId) {
		return this.employeeService.findById(empId).getSalaries();
	}

	@Override
	public Salaries update(Salaries salary) {
		return this.salaryRepository.saveAndFlush(salary);
	}
}
