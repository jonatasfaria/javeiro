package com.javeiro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javeiro.model.Report;
import com.javeiro.repository.ReportRepository;

@Service
public class ReportServiceImp implements ReportService {

	@Autowired
	private ReportRepository reportRepository;
	
	@Override
	public List<Report> getAverageSalaryByGender() {
		return reportRepository.getAverageSalaryByGender();
	}
}
