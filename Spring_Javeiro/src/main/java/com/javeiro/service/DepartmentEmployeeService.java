package com.javeiro.service;

import com.javeiro.model.DepartEmployees;
import com.javeiro.model.Employees;

import java.util.List;

public interface DepartmentEmployeeService {

	DepartEmployees create(int deptId, DepartEmployees departEmp);

	void deleteByDepartment(int deptId);

	void deleteByEmployeeAndDepartment(int empId, int deptId);

	Employees findByEmployeeAndDepartment(int empId, int deptId);

	List<Employees> findByDepartment(int deptId);

	List<DepartEmployees> update(int deptId, DepartEmployees departEmp);

	DepartEmployees update(int deptId, int empId, DepartEmployees departEmp);
}
