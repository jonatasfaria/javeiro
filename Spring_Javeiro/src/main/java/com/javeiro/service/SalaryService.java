package com.javeiro.service;

import java.util.List;

import com.javeiro.model.Salaries;

public interface SalaryService {

	void delete(int id);

	Salaries get(int id);

	Salaries update(Salaries salary);

	List<Salaries> getSalaryByEmployee(int empId);
}
