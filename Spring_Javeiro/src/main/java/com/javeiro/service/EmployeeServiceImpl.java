package com.javeiro.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.javeiro.model.Employees;
import com.javeiro.model.Salaries;
import com.javeiro.model.Titles;
import com.javeiro.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public Employees create(Employees employees) {
		return this.employeeRepository.saveAndFlush(employees);
	}

	@Override
	public void delete(int id) {
		this.employeeRepository.delete(id);
		this.employeeRepository.flush();
	}

	@Override
	public Boolean exists(int id) {
		return this.employeeRepository.exists(id);
	}

	@Override
	public Page<Employees> findAll(Pageable pageable) {
		return this.employeeRepository.findAll(pageable);
	}

	@Override
	public Employees findById(int id) {
		return this.employeeRepository.findOne(id);
	}

	@Override
	public Employees update(Employees employees) {
		return this.employeeRepository.saveAndFlush(employees);
	}

	@Override
	public Employees addSalary(int id, Salaries salary) {
		Employees emp = this.employeeRepository.findOne(id);
		emp.getSalaries().add(salary);
		return this.employeeRepository.saveAndFlush(emp);
	}

	@Override
	public Employees addTitle(int id, Titles title) {
		Employees emp = this.employeeRepository.findOne(id);
		emp.getTitles().add(title);
		return this.employeeRepository.saveAndFlush(emp);
	}

	@Override
	public void validateEmployee(int empId) throws Exception {
		if (!this.exists(empId)) {
			throw new IllegalArgumentException(String.format("Employee not found: %s", empId));
		}
	}
	
	@Override
	public Page<Employees> findByFilter(String filter, Pageable pageable) {
		return this.employeeRepository.findByFilter(filter.toUpperCase(), pageable);
	}
}
