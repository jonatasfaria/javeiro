package com.javeiro.service;

import com.javeiro.model.DepartmentManagers;
import com.javeiro.model.Departments;
import com.javeiro.model.Employees;
import com.javeiro.repository.DepartmentManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by erics on 27/09/2016.
 */
@Service
public class DepartmentManagerServiceImpl implements DepartmentManagerService {

    @Autowired
    private DepartmentsService departmentsService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private DepartmentManagerRepository departmentManagerRepository;

    @Override
    public DepartmentManagers create(int deptId, DepartmentManagers departmentManager) {
        Departments departments = this.departmentsService.findById(deptId);
        Employees employees = this.employeeService.findById(departmentManager.getEmployee().getEmpNo());

        departmentManager.setDepartment(departments);
        departmentManager.setEmployee(employees);

        List<DepartmentManagers> departmentManagers = departments.getDepartmentManager();
        departmentManagers.add(departmentManager);
        departments.setDepartmentManager(departmentManagers);

        List<DepartmentManagers> employeesDepartments = employees.getDepartmentManager();
        employeesDepartments.add(departmentManager);
        employees.setDepartmentManager(employeesDepartments);

        DepartmentManagers departmentManagerResult = this.departmentManagerRepository.saveAndFlush(departmentManager);
        this.employeeService.update(employees);
        Departments update = this.departmentsService.update(departments);
        return departmentManagerResult;
    }

    @Override
    public void deleteByDepartment(int deptId) {
        Departments department = new Departments();
        department.setId(deptId);
        this.departmentManagerRepository.deleteByDepartment(department);
    }

    @Override
    public void deleteByEmployeeAndDepartment(int empId, int deptId) {
        Employees employees = new Employees();
        employees.setEmpNo(empId);

        Departments departments = new Departments();
        departments.setId(deptId);

        this.departmentManagerRepository.deleteByEmployeeAndDepartment(employees, departments);
    }

    @Override
    public Employees findByEmployeeAndDepartment(int empId, int deptId) {
        Employees employees = new Employees();
        employees.setEmpNo(empId);

        Departments departments = new Departments();
        departments.setId(deptId);

        List<DepartmentManagers> departmentManagers = this.departmentManagerRepository.findByEmployeeAndDepartment(employees, departments);
        return departmentManagers.stream().findFirst().get().getEmployee();
    }

    @Override
    public List<Employees> findByDepartment(int deptId) {
        Departments dept = new Departments();
        dept.setId(deptId);

        List<DepartmentManagers> departmentManagers = this.departmentManagerRepository.findByDepartment(dept);
        return departmentManagers.stream().map(q -> q.getEmployee()).collect(Collectors.toList());
    }

    @Override
    public List<DepartmentManagers> update(int deptId, DepartmentManagers departmentManager) {
        List<DepartmentManagers> updatedDepartmentManagers = new ArrayList<DepartmentManagers>();

        Departments departments = this.departmentsService.findById(deptId);

        for (DepartmentManagers departmentManagerEach : this.departmentManagerRepository.findByDepartment(departments)) {

            departmentManagerEach.setFromDate(departmentManager.getFromDate());
            departmentManagerEach.setToDate(departmentManager.getToDate());

            Employees employees = this.employeeService.findById(departmentManagerEach.getEmployee().getEmpNo());

            departmentManagerEach.setDepartment(departments);
            departmentManagerEach.setEmployee(employees);

            List<DepartmentManagers> departmentManagers = departments.getDepartmentManager();
            departmentManagers.add(departmentManagerEach);
            departments.setDepartmentManager(departmentManagers);

            List<DepartmentManagers> employeesDepartmentManager = employees.getDepartmentManager();
            employeesDepartmentManager.add(departmentManagerEach);
            employees.setDepartmentManager(employeesDepartmentManager);

            updatedDepartmentManagers.add(this.departmentManagerRepository.saveAndFlush(departmentManagerEach));
        }

        return updatedDepartmentManagers;
    }

    @Override
    public DepartmentManagers update(int deptId, int empId, DepartmentManagers departmentManager) {
        Departments departments = this.departmentsService.findById(deptId);
        Employees employees = this.employeeService.findById(departmentManager.getEmployee().getEmpNo());

        DepartmentManagers departmentManagerEach = this.departmentManagerRepository.findByEmployeeAndDepartment(employees, departments)
                .stream().findFirst().orElse(null);

        departmentManagerEach.setFromDate(departmentManager.getFromDate());
        departmentManagerEach.setToDate(departmentManager.getToDate());

        departmentManagerEach.setDepartment(departments);
        departmentManagerEach.setEmployee(employees);

        List<DepartmentManagers> departmentManagers = departments.getDepartmentManager();
        departmentManagers.add(departmentManagerEach);
        departments.setDepartmentManager(departmentManagers);

        List<DepartmentManagers> employeesDepartmentManager = employees.getDepartmentManager();
        employeesDepartmentManager.add(departmentManager);
        employees.setDepartmentManager(employeesDepartmentManager);

        return this.departmentManagerRepository.saveAndFlush(departmentManager);
    }
}
