package com.javeiro.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.javeiro.model.Employees;
import com.javeiro.model.Salaries;
import com.javeiro.model.Titles;

public interface EmployeeService {
	Page<Employees> findAll(Pageable pageable);
	
	Employees findById(int id);
	
	void delete(int id);
	
	Employees update(Employees employees);

	Employees create(Employees employees);
	
	Boolean exists(int id);
	
	Employees addSalary(int id, Salaries salary);
	
	Employees addTitle(int id, Titles title);
	
	void validateEmployee(int empId) throws Exception;
	
	Page<Employees> findByFilter(String filter, Pageable pageable);
}
