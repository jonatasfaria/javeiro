package com.javeiro.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.javeiro.model.DepartEmployees;
import com.javeiro.model.Departments;
import com.javeiro.model.Employees;
import com.javeiro.repository.DepartmentEmployeeRepository;
import com.javeiro.repository.DepartmentsRepository;

@Service
public class DepartmentsServiceImpl implements DepartmentsService {

	@Autowired
	private DepartmentsRepository departmentRepository;

	@Override
	public Departments create(Departments department) {
		return this.departmentRepository.saveAndFlush(department);
	}

	@Override
	public void delete(int id) {
		this.departmentRepository.delete(id);
		this.departmentRepository.flush();
	}

	@Override
	public Boolean exists(int id) {
		return this.departmentRepository.exists(id);
	}

	@Override
	public Page<Departments> findAll(Pageable pageable) {
		return departmentRepository.findAll(pageable);
	}

	@Override
	public Departments findById(int id) {
		return departmentRepository.findOne(id);
	}

	@Override
	public Employees getEmployee(int deptId, int empId) {
		// @formatter:off
		Employees emps = this.departmentRepository.findOne(deptId)
												  .getDepartEmployees().stream()
												  .filter(q -> q.getEmployee().getEmpNo() == empId)
												  .map(e -> e.getEmployee())
												  .findFirst()
												  .orElse(null);
		// @formatter:on
		return emps;
	}

	@Override
	public List<Employees> getEmployees(int deptId) {
		// @formatter:off
		List<Employees> emps = this.departmentRepository
								   .findOne(deptId)
								   .getDepartEmployees()
								   .stream()
								   .map(e -> e.getEmployee())
								   .collect(Collectors.toList());

		// @formatter:on
		return emps;
	}

	@Override
	public List<Employees> getManagers(int deptId) {
		// @formatter:off
		List<Employees> managers = this.departmentRepository
									   .findOne(deptId)
									   .getDepartmentManager()
									   .stream()
									   .map(e -> e.getEmployee())
									   .collect(Collectors.toList());

		// @formatter:on
		return managers;
	}

	@Override
	public Departments update(Departments department) {
		return this.departmentRepository.saveAndFlush(department);
	}

	@Override
	public void validate(int deptId) throws Exception {
		if (!this.exists(deptId)) {
			throw new IllegalArgumentException(String.format("Department not found: %s", deptId));
		}
	}
}
