package com.javeiro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.javeiro.model.Employees;
import com.javeiro.model.Report;

@Repository
public interface ReportRepository extends JpaRepository<Employees, Integer> {

	@Query("SELECT new com.javeiro.model.Report(E.gender, AVG(S.salary)) FROM Employees E JOIN E.salaries S WHERE S.toDate = '9999-01-01' GROUP BY E.gender")
	public List<Report> getAverageSalaryByGender();
}