package com.javeiro.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.javeiro.model.Employees;

@Repository
public interface EmployeeRepository extends JpaRepository<Employees, Integer>, PagingAndSortingRepository<Employees,Integer> {

	@Query("SELECT e FROM Employees e WHERE UPPER(first_name) LIKE %:filter% OR UPPER(last_name) LIKE %:filter%")
	public Page<Employees> findByFilter(@Param("filter") String filter, Pageable pageable);
}