package com.javeiro.repository;

import com.javeiro.model.DepartmentManagers;
import com.javeiro.model.Departments;
import com.javeiro.model.Employees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentManagerRepository extends JpaRepository<DepartmentManagers, Integer> {

    List<DepartmentManagers> findByEmployeeAndDepartment(Employees emp, Departments department);

    List<DepartmentManagers> findByDepartment(Departments department);

    long deleteByEmployeeAndDepartment(Employees emp, Departments department);

    long deleteByDepartment(Departments department);
}

