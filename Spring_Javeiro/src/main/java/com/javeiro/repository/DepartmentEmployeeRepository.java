package com.javeiro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.javeiro.model.DepartEmployees;
import com.javeiro.model.Departments;
import com.javeiro.model.Employees;

@Repository
public interface DepartmentEmployeeRepository extends JpaRepository<DepartEmployees, Integer> {
	
	List<DepartEmployees> findByEmployeeAndDepartment(Employees emp, Departments department);
	
	List<DepartEmployees> findByDepartment(Departments department);
	
	long deleteByEmployeeAndDepartment(Employees emp, Departments department);
	
	long deleteByDepartment(Departments department);	
}
