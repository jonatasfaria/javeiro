package com.javeiro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.javeiro.model.Salaries;

@Repository
public interface SalaryRepository extends JpaRepository<Salaries, Integer> {

}
