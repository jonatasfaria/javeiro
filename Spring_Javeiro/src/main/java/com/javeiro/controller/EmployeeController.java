package com.javeiro.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.javeiro.DTO.Employee;
import com.javeiro.config.Map;
import com.javeiro.model.Employees;
import com.javeiro.service.EmployeeService;

@RestController
@RequestMapping("/v1/employees")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(method = RequestMethod.POST)
	public Employees create(@RequestBody Employees employees) {
		return this.employeeService.create(employees);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable int id) {
		this.employeeService.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET)
	public Page<Employee> get(Pageable pageable) {
		Page<Employees> emps = this.employeeService.findAll(pageable);

		List<Employee> contents = emps.getContent().stream().map(a -> Map.getModelMapper().map(a, Employee.class))
				.collect(Collectors.toList());

		return new PageImpl<Employee>(contents, pageable, emps.getTotalElements());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Employee get(@PathVariable int id) {
		Employees emp = this.employeeService.findById(id);

		Employee dto = Map.getModelMapper().map(emp, Employee.class);

		return dto;
	}

	@RequestMapping(method = RequestMethod.PUT)
	public Employees update(@RequestBody Employees employees) {
		return this.employeeService.update(employees);
	}
	
	@RequestMapping(value = "/filter", method = RequestMethod.GET)
	public Page<Employee> findByFilter(Pageable pageable, String filter) {
		
		Page<Employees> emps = this.employeeService.findByFilter(filter,pageable);

		List<Employee> contents = emps.getContent().stream().map(a -> Map.getModelMapper().map(a, Employee.class))
				.collect(Collectors.toList());

		return new PageImpl<Employee>(contents, pageable, emps.getTotalElements());
	}
}
