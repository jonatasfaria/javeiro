package com.javeiro.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javeiro.model.Titles;
import com.javeiro.service.EmployeeService;
import com.javeiro.service.TitleService;

@RestController
@RequestMapping("/v1/employees/{empId}/titles")
public class EmployeeTitleController {

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private TitleService titleService;

	@RequestMapping(method = RequestMethod.POST)
	public Titles create(@PathVariable int empId, @RequestBody Titles title) throws Exception {
		this.employeeService.validateEmployee(empId);
		List<Titles> titles = this.employeeService.addTitle(empId, title).getTitles();
		return titles.get(titles.size() - 1);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable int empId, @PathVariable int id) throws Exception {
		this.employeeService.validateEmployee(empId);
		this.titleService.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<Titles> get(@PathVariable int empId) throws Exception {
		this.employeeService.validateEmployee(empId);
		return this.titleService.getTitleByEmployee(empId);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Titles get(@PathVariable int empId, @PathVariable int id) throws Exception {
		this.employeeService.validateEmployee(empId);
		return this.titleService.get(empId, id);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public Titles update(@PathVariable int empId, @RequestBody Titles title) throws Exception {
		this.employeeService.validateEmployee(empId);
		return this.titleService.update(title);
	}
}
