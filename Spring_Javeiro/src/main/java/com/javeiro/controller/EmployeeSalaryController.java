package com.javeiro.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javeiro.model.Employees;
import com.javeiro.model.Salaries;
import com.javeiro.service.EmployeeService;
import com.javeiro.service.SalaryService;

@RestController
@RequestMapping("/v1/employees/{empId}/salaries")
public class EmployeeSalaryController {

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private SalaryService salaryService;

	@RequestMapping(method = RequestMethod.POST)
	public Salaries create(@PathVariable int empId, @RequestBody Salaries salary) throws Exception {
		this.employeeService.validateEmployee(empId);
		List<Salaries> salaries = this.employeeService.addSalary(empId, salary).getSalaries();
		return salaries.get(salaries.size() - 1);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable int empId, @PathVariable int id) throws Exception {
		this.employeeService.validateEmployee(empId);
		this.salaryService.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<Salaries> get(@PathVariable int empId) throws Exception {
		this.employeeService.validateEmployee(empId);
		return this.salaryService.getSalaryByEmployee(empId);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Salaries get(@PathVariable int empId, @PathVariable int id) throws Exception {
		this.employeeService.validateEmployee(empId);
		return this.salaryService.get(id);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public Salaries update(@PathVariable int empId, @RequestBody Salaries salary) throws Exception {
		this.employeeService.validateEmployee(empId);
		return this.salaryService.update(salary);
	}
}
