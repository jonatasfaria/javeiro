package com.javeiro.controller;

import com.javeiro.DTO.Department;
import com.javeiro.DTO.DepartmentEmployee;
import com.javeiro.DTO.Employee;
import com.javeiro.config.Map;
import com.javeiro.model.DepartEmployees;
import com.javeiro.model.Employees;
import com.javeiro.service.DepartmentEmployeeService;
import com.javeiro.service.DepartmentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v1/departments/{deptId}/employees")
public class DepartmentEmployeeController {

	@Autowired
	private DepartmentsService departmentService;

	@Autowired
	private DepartmentEmployeeService departmentEmployeeService;

	@RequestMapping(method = RequestMethod.POST)
	public Department create(@PathVariable int deptId, @RequestBody DepartmentEmployee departEmp) throws Exception {
		this.departmentService.validate(deptId);

		DepartEmployees departEmps = this.departmentEmployeeService.create(deptId,
				Map.getModelMapper().map(departEmp, DepartEmployees.class));

		return Map.getModelMapper().map(departEmps.getDepartment(), Department.class);
	}

	@RequestMapping(method = RequestMethod.DELETE)
	public void delete(@PathVariable int deptId) throws Exception {
		this.departmentService.validate(deptId);
		this.departmentEmployeeService.deleteByDepartment(deptId);
	}

	@RequestMapping(value = "/{empId}", method = RequestMethod.DELETE)
	public void delete(@PathVariable int empId, @PathVariable int deptId) throws Exception {
		this.departmentService.validate(deptId);
		this.departmentEmployeeService.deleteByEmployeeAndDepartment(empId, deptId);
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<Employee> get(@PathVariable int deptId) throws Exception {
		this.departmentService.validate(deptId);

		List<Employees> emps = this.departmentEmployeeService.findByDepartment(deptId);

		// @formatter:off
		List<Employee> dto = emps.stream()
								 .map(a -> Map.getModelMapper().map(a, Employee.class))
								 .collect(Collectors.toList());
		
		// @formatter:on

		return dto;
	}

	@RequestMapping(value = "/{empId}", method = RequestMethod.GET)
	public Employee get(@PathVariable int deptId, @PathVariable int empId) throws Exception {
		this.departmentService.validate(deptId);

		return Map.getModelMapper().map(this.departmentEmployeeService.findByEmployeeAndDepartment(empId, deptId),
				Employee.class);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public List<Employee> update(@PathVariable int deptId, @RequestBody DepartmentEmployee deptEmp) throws Exception {
		this.departmentService.validate(deptId);

		List<DepartEmployees> departmentEmployees = this.departmentEmployeeService.update(deptId,
				Map.getModelMapper().map(deptEmp, DepartEmployees.class));

		// @formatter:off
		List<Employee> dto = departmentEmployees.stream()
				 								.map(a -> Map.getModelMapper().map(a.getEmployee(), Employee.class))
				 								.collect(Collectors.toList());
		// @formatter:off
		return dto;
	}
	
	@RequestMapping(value = "/{empId}", method = RequestMethod.PUT)
	public List<Employee> update(@PathVariable int deptId, @PathVariable int empId, @RequestBody DepartmentEmployee deptEmp) throws Exception {
		this.departmentService.validate(deptId);

		List<DepartEmployees> departmentEmployees = this.departmentEmployeeService.update(deptId,
				Map.getModelMapper().map(deptEmp, DepartEmployees.class));

		// @formatter:off
		List<Employee> dto = departmentEmployees.stream()
				 								.map(a -> Map.getModelMapper().map(a.getEmployee(), Employee.class))
				 								.collect(Collectors.toList());
		// @formatter:off
		return dto;
	}
	
}
