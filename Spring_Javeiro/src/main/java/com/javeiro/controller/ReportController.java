package com.javeiro.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javeiro.model.Report;
import com.javeiro.service.ReportService;

@RestController
@RequestMapping("/v1/reports")
public class ReportController {

	@Autowired
	private ReportService reportService;

	@RequestMapping(value = "/getAverageSalaryByGender", method = RequestMethod.GET)
	public List<Report> getAverageSalaryByGender() throws Exception {
		return reportService.getAverageSalaryByGender();
	}
}
