package com.javeiro.controller;

import com.javeiro.DTO.Department;
import com.javeiro.config.Map;
import com.javeiro.model.Departments;
import com.javeiro.service.DepartmentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v1/departments")
public class DepartmentController {
	
	@Autowired
	private DepartmentsService departmentsService;
	
	@RequestMapping(method = RequestMethod.POST)
	public Departments create(@RequestBody Departments departments) {
		return this.departmentsService.create(departments);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable int id) {
		this.departmentsService.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET)
	public Page<Department> get(Pageable pageable) {
		Page<Departments> depts = this.departmentsService.findAll(pageable);
		
		List<Department> contents = depts.getContent().stream().map(a -> Map.getModelMapper().map(a, Department.class))
				.collect(Collectors.toList());

		return new PageImpl<Department>(contents, pageable, depts.getTotalElements());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Department get(@PathVariable int id) {
		Departments departs = this.departmentsService.findById(id);
		
		Department dto = Map.getModelMapper().map(departs, Department.class);
		
		return dto;		
	}

	@RequestMapping(method = RequestMethod.PUT)
	public Departments update(@RequestBody Departments departments) {
		return this.departmentsService.update(departments);
	}
}
