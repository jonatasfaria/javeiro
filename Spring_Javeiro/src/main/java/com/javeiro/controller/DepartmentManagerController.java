package com.javeiro.controller;

import com.javeiro.DTO.Department;
import com.javeiro.DTO.DepartmentEmployee;
import com.javeiro.DTO.DepartmentManager;
import com.javeiro.DTO.Employee;
import com.javeiro.config.Map;
import com.javeiro.model.DepartmentManagers;
import com.javeiro.model.Employees;
import com.javeiro.service.DepartmentManagerService;
import com.javeiro.service.DepartmentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by erics on 27/09/2016.
 */
@RestController
@RequestMapping("/v1/departments/{deptId}/managers")
public class DepartmentManagerController {
    @Autowired
    private DepartmentsService departmentService;

    @Autowired
    private DepartmentManagerService departmentManagerService;

    @RequestMapping(method = RequestMethod.POST)
    public Department create(@PathVariable int deptId, @RequestBody DepartmentManager departmentManager) throws Exception {
        this.departmentService.validate(deptId);

        DepartmentManagers departEmps = this.departmentManagerService.create(deptId,
                Map.getModelMapper().map(departmentManager, DepartmentManagers.class));

        return Map.getModelMapper().map(departEmps.getDepartment(), Department.class);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@PathVariable int deptId) throws Exception {
        this.departmentService.validate(deptId);
        this.departmentManagerService.deleteByDepartment(deptId);
    }

    @RequestMapping(value = "/{empId}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int empId, @PathVariable int deptId) throws Exception {
        this.departmentService.validate(deptId);
        this.departmentManagerService.deleteByEmployeeAndDepartment(empId, deptId);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Employee> get(@PathVariable int deptId) throws Exception {
        this.departmentService.validate(deptId);

        List<Employees> emps = this.departmentManagerService.findByDepartment(deptId);

        // @formatter:off
        List<Employee> dto = emps.stream()
                .map(a -> Map.getModelMapper().map(a, Employee.class))
                .collect(Collectors.toList());

        // @formatter:on

        return dto;
    }

    @RequestMapping(value = "/{empId}", method = RequestMethod.GET)
    public Employee get(@PathVariable int deptId, @PathVariable int empId) throws Exception {
        this.departmentService.validate(deptId);

        return Map.getModelMapper().map(this.departmentManagerService.findByEmployeeAndDepartment(empId, deptId),
                Employee.class);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public List<Employee> update(@PathVariable int deptId, @RequestBody DepartmentEmployee deptEmp) throws Exception {
        this.departmentService.validate(deptId);

        List<DepartmentManagers> departmentEmployees = this.departmentManagerService.update(deptId,
                Map.getModelMapper().map(deptEmp, DepartmentManagers.class));

        // @formatter:off
        List<Employee> dto = departmentEmployees.stream()
                .map(a -> Map.getModelMapper().map(a.getEmployee(), Employee.class))
                .collect(Collectors.toList());
        // @formatter:off
        return dto;
    }

    @RequestMapping(value = "/{empId}", method = RequestMethod.PUT)
    public List<Employee> update(@PathVariable int deptId, @PathVariable int empId, @RequestBody DepartmentEmployee deptEmp) throws Exception {
        this.departmentService.validate(deptId);

        List<DepartmentManagers> departmentEmployees = this.departmentManagerService.update(deptId,
                Map.getModelMapper().map(deptEmp, DepartmentManagers.class));

        // @formatter:off
        List<Employee> dto = departmentEmployees.stream()
                .map(a -> Map.getModelMapper().map(a.getEmployee(), Employee.class))
                .collect(Collectors.toList());
        // @formatter:off
        return dto;
    }

}

