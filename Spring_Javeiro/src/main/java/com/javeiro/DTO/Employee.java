package com.javeiro.DTO;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.javeiro.model.DepartEmployees;
import com.javeiro.model.Departments;
import com.javeiro.model.Salaries;
import com.javeiro.model.Titles;

public class Employee {

	private Date birthDate;
	private List<Departments> departmentsEmployee = new ArrayList<Departments>();
	private List<Departments> departmentsManager = new ArrayList<Departments>();
	private Integer empNo;
	private String firstName;
	private String gender;
	private Date hireDate;
	private String lastName;
	private List<Salaries> salaries = new ArrayList<Salaries>();
	private List<Titles> titles;
	
	public Date getBirthDate() {
		return birthDate;
	}

	public List<Departments> getDepartmentsEmployee() {
		return departmentsEmployee;
	}

	public List<Departments> getDepartmentsManager() {
		return departmentsManager;
	}

	public Integer getEmpNo() {
		return empNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getGender() {
		return gender;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public String getLastName() {
		return lastName;
	}

	public List<Salaries> getSalaries() {
		return salaries;
	}

	public List<Titles> getTitles() {
		return titles;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public void setDepartmentsEmployee(List<Departments> departmentsEmployee) {
		this.departmentsEmployee = departmentsEmployee;
	}

	public void setDepartmentsManager(List<Departments> departmentsManager) {
		this.departmentsManager = departmentsManager;
	}

	public void setEmpNo(Integer empNo) {
		this.empNo = empNo;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setSalaries(List<Salaries> salaries) {
		this.salaries = salaries;
	}
	public void setTitles(List<Titles> titles) {
		this.titles = titles;
	}
}
