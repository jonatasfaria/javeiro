package com.javeiro.DTO;

import com.javeiro.model.Employees;

import java.util.List;

public class Department {
	
	private String deptName;	
	private String deptNo;	
	private List<Employees> employees;	
	private int id;	
	private List<Employees> managers;
	
	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public List<Employees> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employees> employees) {
		this.employees = employees;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Employees> getManagers() {
		return managers;
	}

	public void setManagers(List<Employees> managers) {
		this.managers = managers;
	}
}
