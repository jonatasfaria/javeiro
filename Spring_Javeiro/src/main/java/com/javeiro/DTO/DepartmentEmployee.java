package com.javeiro.DTO;

import java.sql.Date;

import com.javeiro.model.Departments;
import com.javeiro.model.Employees;

public class DepartmentEmployee {

	private Departments department;
	private int dept_id_emp;
	private Employees employee;
	private Date fromDate;
	private Date toDate;

	public Departments getDepartment() {
		return department;
	}

	public int getDept_id_emp() {
		return dept_id_emp;
	}

	public Employees getEmployee() {
		return employee;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setDepartment(Departments department) {
		this.department = department;
	}

	public void setDept_id_emp(int dept_id_emp) {
		this.dept_id_emp = dept_id_emp;
	}

	public void setEmployee(Employees employee) {
		this.employee = employee;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
}
