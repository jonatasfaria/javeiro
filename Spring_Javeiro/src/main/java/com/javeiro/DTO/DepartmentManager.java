package com.javeiro.DTO;

import com.javeiro.model.Departments;
import com.javeiro.model.Employees;

import java.sql.Date;

/**
 * Created by erics on 27/09/2016.
 */
public class DepartmentManager {
    private Departments department;
    private int dept_id_emp;
    private Employees employee;
    private Date fromDate;
    private Date toDate;

    public Departments getDepartment() {
        return department;
    }

    public void setDepartment(Departments department) {
        this.department = department;
    }

    public int getDept_id_emp() {
        return dept_id_emp;
    }

    public void setDept_id_emp(int dept_id_emp) {
        this.dept_id_emp = dept_id_emp;
    }

    public Employees getEmployee() {
        return employee;
    }

    public void setEmployee(Employees employee) {
        this.employee = employee;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
}
